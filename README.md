# GashAdmin

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Heroku Deploy

Review remote branch:
    git remote -v

If Heroku Git URL does not appear:
    git remote add <remote-branch-name>  https://git.heroku.com/tir-admin-stging.git

Create new branch:
    git checkout -b <branch-name>

Remove `dist` from `.gitignore`.

Add into `environments/environment.prod.ts` -> apiUrl: 'https://tir-api-staging.herokuapp.com/'.

Change `"start": "ng serve -o"` in package.json to `"start": "node server.js"`.

Add `"postinstall": "ng build --prod"`.

Add
    `"@angular-devkit/build-angular"` 
    `"@angular/cli"`
    `"@angular/compiler-cli"`
into `dependencies` from `devDependencies`.

To finish
    git add .
    git commit -m "Heroku deployment"
    git push <remote-branch-name> <branch-name>:master -f


