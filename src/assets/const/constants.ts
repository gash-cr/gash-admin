export const REVISION_STORAGE_KEY = 'current_revision';

export const EVERGREEN_GENERAL_INFO = [
  { directive: 'createdAt.toLocaleDateString("en-GB")', coordinateX: 14, coordinateY: 39 },
  { directive: 'createdAt.toLocaleTimeString()', coordinateX: 43, coordinateY: 39 },
  { directive: 'revision.containerCode', coordinateX: 70, coordinateY: 39 },
  { directive: 'revision.chassis', coordinateX: 117 , coordinateY: 39 },
  { directive: 'revision.origin', coordinateX: 148, coordinateY: 39 },

  { directive: 'revision.shipperName', coordinateX: 35, coordinateY: 55 },
  { directive: 'revision.travelNumber', coordinateX: 110, coordinateY: 55 },
  { directive: 'revision.measure', coordinateX: 186, coordinateY: 55 },

  { directive: 'revision.origin', coordinateX: 27, coordinateY: 73 },
  { directive: 'revision.boatName', coordinateX: 157, coordinateY: 73 },

  { directive: 'revision.user', coordinateX: 177, coordinateY: 89 },
  { directive: 'revision.destiny', coordinateX: 142, coordinateY: 89 },
];

export const GENERAL_INFO = [
  { directive: 'createdAt.toLocaleDateString("en-GB")', coordinateX: 14, coordinateY: 36 },
  { directive: 'createdAt.toLocaleTimeString()', coordinateX: 43, coordinateY: 36 },
  { directive: 'revision.containerCode', coordinateX: 70, coordinateY: 36 },
  { directive: 'revision.chassis', coordinateX: 117 , coordinateY: 36 },
  { directive: 'revision.origin', coordinateX: 149, coordinateY: 36 },
  { directive: 'revision.destiny', coordinateX: 175, coordinateY: 36 },

  { directive: 'revision.truck', coordinateX: 25, coordinateY: 49 },
  { directive: 'revision.shipperName', coordinateX: 65, coordinateY: 49 },
  { directive: 'revision.driver', coordinateX: 110, coordinateY: 49 },
  { directive: 'revision.measure', coordinateX: 168, coordinateY: 49 },

  // { directive: 'revision.origin', coordinateX: 27, coordinateY: 73 },
  // { directive: 'revision.boatName', coordinateX: 157, coordinateY: 73 },

  // { directive: 'revision.user', coordinateX: 177, coordinateY: 89 },
  // { directive: 'revision.destiny', coordinateX: 142, coordinateY: 89 },
];

export const GASH_PARTS = [
  { id: 'left_side', name: 'Lado Izquierdo', xCoord: 12, yCoord: 47, width: 60, height: 60 },
  { id: 'inside', name: 'Contenedor Interior', xCoord: 76, yCoord: 47, width: 60, height: 60 },
  { id: 'right_side', name: 'Lado Derecho', xCoord: 139, yCoord: 47, width: 60, height: 60 },
  { id: 'front', name: 'Frente', xCoord: 10, yCoord: 110, width: 60, height: 60 },
  { id: 'roof', name: 'Techo', xCoord: 74, yCoord: 125, width: 60, height: 30 },
  { id: 'back', name: 'Parte Trasera', xCoord: 139, yCoord: 110, width: 60, height: 60 },
  { id: 'chassis_up', name: 'Chasis - Arriba', xCoord: 12, yCoord: 172, width: 80, height: 35 },
  { id: 'chassis_side', name: 'Chasis - Costado', xCoord: 12, yCoord: 202, width: 80, height: 25 },
  { id: 'chassis_front', name: 'Chasis - Frente', xCoord: 99, yCoord: 172, width: 50, height: 45 },
  { id: 'chassis_back', name: 'Chasis - Atrás', xCoord: 150, yCoord: 172, width: 45, height: 45 }
];

export const EVERGREEN_PARTS = [
  { id: 'left_evergreen', name: 'Contenedor Izquierdo', xCoord: 22, yCoord: 117, width: 60, height: 60 },
  { id: 'inside_evergreen', name: 'Contenedor Interior', xCoord: 86, yCoord: 117, width: 60, height: 60 },
  { id: 'right_evergreen', name: 'Contenedor Derecho', xCoord: 145, yCoord: 117, width: 60, height: 60 },
  { id: 'chassis_side', name: 'Chasis - Costado', xCoord: 32, yCoord: 172, width: 80, height: 40 },
  { id: 'chassis_up', name: 'Chasis - Arriba', xCoord: 32, yCoord: 195, width: 80, height: 35 },
  { id: 'chassis_front', name: 'Chasis - Frente', xCoord: 109, yCoord: 172, width: 45, height: 55 },
  { id: 'chassis_back', name: 'Chasis - Atrás', xCoord: 161, yCoord: 172, width: 45, height: 55 }
];

export const NYK_PARTS = [
  { id: 'left_side', name: 'Lado Izquierdo', xCoord: 12, yCoord: 120, width: 60, height: 60 },
  { id: 'inside', name: 'Contenedor Interior', xCoord: 76, yCoord: 120, width: 60, height: 60 },
  { id: 'right_side', name: 'Lado Derecho', xCoord: 139, yCoord: 120, width: 60, height: 60 },
  { id: 'front', name: 'Frente', xCoord: 11, yCoord: 170, width: 60, height: 60 },
  { id: 'back', name: 'Parte Trasera', xCoord: 76, yCoord: 170, width: 60, height: 60 },
  { id: 'roof', name: 'Techo', xCoord: 139, yCoord: 170, width: 60, height: 30 },
  { id: 'floor', name: 'Suelo', xCoord: 139, yCoord: 200, width: 60, height: 30 }
];

export const defaultUser = { name: " ", password: " ", isAdmin: true, shippingCompany: "Gash", username: "Gash" };

export const defaultRevision = {
  chassisDetail: [
    { name: " ", model: false },
    { name: " ", model: false },
    { name: " ", model: false },
    { name: " ", model: false },
    { name: " ", model: false }
  ],
  part: [
    { name: " ", in: false, out: false },
    { name: " ", in: false, out: false },
    { name: " ", in: false, out: false },
    { name: " ", in: false, out: false },
    { name: " ", in: false, out: false },
    { name: " ", in: false, out: false },
    { name: " ", in: false, out: false },
    { name: " ", in: false, out: false },
    { name: " ", in: false, out: false },
    { name: " ", in: false, out: false },
    { name: " ", in: false, out: false },
    { name: " ", in: false, out: false },
    { name: " ", in: false, out: false },
    { name: " ", in: false, out: false },
    { name: " ", in: false, out: false },
    { name: " ", in: false, out: false }
  ],
  damages: [
    { damage: { abbreviation: "R", name: "Rayón", image_ref: null }, formName: "Gash", damagedPartId: "left_side", coordinateX: 171, coordinateY: 221 },
    { damage: { abbreviation: "A", name: "Abolladura", image_ref: null }, formName: "Gash", damagedPartId: "inside", coordinateX: 400, coordinateY: 329 },
    { damage: { abbreviation: "H", name: "Hueco", image_ref: null }, formName: "Gash", damagedPartId: "right_side", coordinateX: 448, coordinateY: 272 },
    { damage: { abbreviation: "R", name: "Rayón", image_ref: null }, formName: "Gash", damagedPartId: "chassis_front", coordinateX: 317, coordinateY: 71 },
    { damage: { abbreviation: "A", name: "Abolladura", image_ref: null }, formName: "Gash", damagedPartId: "chassis_back", coordinateX: 124, coordinateY: 281 },
    { damage: { abbreviation: "A", name: "Abolladura", image_ref: null }, formName: "Gash", damagedPartId: "roof", coordinateX: 285, coordinateY: 149 },
    { damage: { abbreviation: "H", name: "Hueco", image_ref: null }, formName: "Gash", damagedPartId: "chassis_up", coordinateX: 431, coordinateY: 160 },
    { damage: { abbreviation: "P", name: "Parche", image_ref: null }, formName: "Gash", damagedPartId: "chassis_side", coordinateX: 549, coordinateY: 82 },
    { damage: { abbreviation: "A", name: "Abolladura", image_ref: null }, formName: "Gash", damagedPartId: "front", coordinateX: 359, coordinateY: 280 },
    { damage: { abbreviation: "A", name: "Abolladura", image_ref: null }, formName: "Gash", damagedPartId: "back", coordinateX: 437, coordinateY: 295 }
  ],
  customsMark1: " ",
  customsMark2: " ",
  customsMark3: " ",
  customsMark4: " ",
  customsMark5: " ",
  customsMark6: " ",
  tire1: " ",
  tire2: " ",
  tire3: " ",
  tire4: " ",
  tire5: " ",
  tire6: " ",
  createdAt: new Date(),
  user: " ",
  shipperName: "Gash",
  containerCode: " ",
  boatName: "",
  travelNumber: "",
  shippingDetail: " ",
  shipping: " ",
  chassis: " ",
  chassisType: " ",
  truck: " ",
  prefix: " ",
  driver: " ",
  guideNumber: " ",
  containerType: " ",
  measure: " ",
  origin: " ",
  destiny: " ",
  genSet: false,
  tiresCondition: " ",
  observations: " ",
  updatedAt: 1548928194,
}

export function getShipperParts(shipper) {
  return eval(`${shipper.toUpperCase()}_PARTS`);
}
