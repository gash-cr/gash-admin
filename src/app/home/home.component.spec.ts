import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { HomeComponent } from './home.component'
import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'
import { NavComponent } from '../nav/nav.component'
import { AngularFontAwesomeModule } from 'angular-font-awesome'
import { RouterTestingModule } from '@angular/router/testing'
import { LockerModule } from 'angular-safeguard'
import { StorageServiceModule } from 'angular-webstorage-service'

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomeComponent, NavComponent],
      imports: [AngularFontAwesomeModule, RouterTestingModule, LockerModule, StorageServiceModule],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
