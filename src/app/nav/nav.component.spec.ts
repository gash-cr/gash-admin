import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { AuthGuard } from '../authGuard/auth-guard'
import { LockerModule } from 'angular-safeguard'
import { AngularFontAwesomeModule } from 'angular-font-awesome'
import { NavComponent } from './nav.component'
import { RouterTestingModule } from '@angular/router/testing'
import { StorageServiceModule } from 'angular-webstorage-service'

describe('NavComponent', () => {
  let component: NavComponent;
  let fixture: ComponentFixture<NavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NavComponent],
      imports: [LockerModule, AngularFontAwesomeModule, RouterTestingModule, StorageServiceModule],
      providers: [AuthGuard]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
