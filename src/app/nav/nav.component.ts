import { Component, OnInit, Inject } from '@angular/core';
import { Locker, DRIVERS } from 'angular-safeguard';
import { Router } from '@angular/router';
import { StorageService, SESSION_STORAGE } from 'angular-webstorage-service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.less']
})

export class NavComponent implements OnInit {

  shippName;
  constructor(
    private router: Router,
    private locker: Locker,
    @Inject(SESSION_STORAGE) private storage: StorageService
  ) { }

  ngOnInit(): void {
    this.shippName = this.storage.get('shipper');
  }

  logOut() {
    this.locker.clear(DRIVERS.SESSION);
    this.router.navigateByUrl('/login');
  }

  goToUsers() {
    this.router.navigate(['/users']);
  }

  goToRevisions() {
    this.router.navigate(['/revision']);
  }
}