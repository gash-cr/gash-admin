import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../user-service/user.service';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-user-modal',
  templateUrl: './user-modal.component.html',
  styleUrls: ['../../../theme/styles/user-styles.less']
})

export class UserModalComponent implements OnInit {
  userForm: FormGroup;
  loading = false;
  submitted;

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private toastr: ToastrService,
              public bsModalRef: BsModalRef) { }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      username: ['', Validators.required],
      name: ['', Validators.required],
      password: ['', Validators.required],
      shippingCompany: [''],
    });
  }

  get form() { return this.userForm.controls; }

  onSubmit() {

    this.submitted = true;
    const user = {
      username: this.form.username.value,
      isAdmin: false,
      password: this.form.password.value,
      name: this.form.name.value,
      shippingCompany: this.form.shippingCompany.value,
    };

    if (this.userForm.invalid) {
      return;
    }

    this.userService.create(user)
      .subscribe((res) => {
        this.toastr.success('El usuario ha sido creado correctamente', 'Usuario Registrado');
        this.closeDialog();
      },
        (err) => { this.toastr.error('El usuario ya existe', 'Usuario Registrado'); });
  }

  closeDialog() {
    this.bsModalRef.hide();
  }
}
