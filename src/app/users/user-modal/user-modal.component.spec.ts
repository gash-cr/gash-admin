import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { UserModalComponent } from './user-modal.component'
import { ModalModule, BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'
import { HttpClientModule } from '@angular/common/http'
import { ToastrModule } from 'ngx-toastr'

describe('UserModalComponent', () => {
  let component: UserModalComponent;
  let fixture: ComponentFixture<UserModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserModalComponent],
      imports: [ModalModule.forRoot(), FormsModule, ReactiveFormsModule, HttpClientModule, ToastrModule.forRoot()],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
      providers: [BsModalRef, BsModalService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
