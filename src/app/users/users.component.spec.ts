import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { UsersComponent } from './users.component'
import { NavComponent } from '../nav/nav.component'
import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'
import { MatTableModule } from '@angular/material'
import { HttpClientModule } from '@angular/common/http'
import { ModalModule, BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import { RouterTestingModule } from '@angular/router/testing'
import { AuthGuard } from '../authGuard/auth-guard'
import { LockerModule } from 'angular-safeguard'
import { StorageServiceModule } from 'angular-webstorage-service'

describe('UsersComponent', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UsersComponent, NavComponent],
      imports: [StorageServiceModule, LockerModule, MatTableModule, HttpClientModule, ModalModule.forRoot(), RouterTestingModule],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
      providers: [ModalModule, BsModalRef, BsModalService, AuthGuard]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
