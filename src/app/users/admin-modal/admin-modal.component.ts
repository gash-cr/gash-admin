import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../user-service/user.service';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-admin-modal',
  templateUrl: './admin-modal.component.html',
  styleUrls: ['../../../theme/styles/user-styles.less']
})

export class AdminModalComponent implements OnInit {

  adminUserForm: FormGroup;
  submitted: boolean;
  selectedShipper;

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private toastr: ToastrService,
              public bsModalRef: BsModalRef) { }

  ngOnInit() {
    this.adminUserForm = this.formBuilder.group({
      adminEmail: ['', [Validators.required, Validators.email]],
      adminPassword: ['', Validators.required],
      shippingCompany: ['Gash', Validators.required],
    });
  }

  get form() { return this.adminUserForm.controls; }

  onSubmit() {
    this.submitted = true;
    const user = {
      username: this.form.adminEmail.value,
      isAdmin: true,
      password: this.form.adminPassword.value,
      name: this.form.adminEmail.value,
      shippingCompany: this.form.shippingCompany.value
    };

    if (this.adminUserForm.invalid) {
      return;
    }

    this.userService.create(user)
      .subscribe((res) => {
        this.toastr.success('El usuario ha sido creado correctamente', 'Nuevo Usuario Registrado');
        this.closeDialog();
      },
        (err) => {
          this.toastr.error('El usuario ya existe', 'Usuario Registrado');
        }
      );
  }

  closeDialog() {
    this.bsModalRef.hide();
  }
}
