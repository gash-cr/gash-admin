import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../../models/User';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class UserService {
  returnedData;
  userUrl = environment.apiUrl + 'users';

  constructor(private http: HttpClient) { }

  public create(user: User): Observable<User> {
    return this.http.post<User>(this.userUrl, user);
  }

  public get() {
    return this.http.get(this.userUrl);
  }

  public delete(id) {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }), body: { _id: id }
    };
    return this.http.delete(this.userUrl, httpOptions).subscribe();
  }

  public update(user) {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }), body: { _id: user._id }
    };
    return this.http.put<User>(this.userUrl, user, httpOptions).subscribe();
  }
}