import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { UserEditModalComponent } from './user-edit-modal.component'
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'
import { HttpClientModule } from '@angular/common/http'
import { ToastrModule } from 'ngx-toastr'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'


describe('UserEditModalComponent', () => {
  let component: UserEditModalComponent;
  let fixture: ComponentFixture<UserEditModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserEditModalComponent],
      imports: [HttpClientModule, FormsModule, ReactiveFormsModule, ToastrModule.forRoot()],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
      providers: [BsModalRef, BsModalService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserEditModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
