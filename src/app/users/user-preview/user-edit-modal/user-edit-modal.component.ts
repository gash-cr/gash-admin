import { Component, OnInit } from '@angular/core'
import { UserService } from '../../user-service/user.service'
import { ToastrService } from 'ngx-toastr'
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { BsModalRef } from 'ngx-bootstrap/modal'
import { defaultUser } from '../../../../assets/const/constants'

@Component({
  selector: 'app-user-edit-modal',
  templateUrl: './user-edit-modal.component.html',
  styleUrls: ['../../../../theme/styles/user-styles.less']
})

export class UserEditModalComponent implements OnInit {
  editUserForm: FormGroup;
  loading = false;
  submitted;
  user;

  constructor(private formBuilder: FormBuilder,
    private userService: UserService,
    private toastr: ToastrService,
    public bsModalRef: BsModalRef
  ) { }

  ngOnInit() {
    this.user = (this.user) ? this.user : defaultUser;
    this.editUserForm = this.formBuilder.group({
      username: [this.user.username, Validators.required],
      name: [this.user.name, Validators.required],
      password: ['']
    });
  }

  get form() { return this.editUserForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.editUserForm.invalid) {
      return;
    }

    this.user.name = this.form.name.value;
    this.user.username = this.form.username.value;
    this.user.password = this.form.password.value;
    this.user.isPasswordNew = (this.form.password.value !== '');
    this.userService.update(this.user);
    this.toastr.success('El usuario ha sido editado correctamente', 'Usuario editado');
    this.closeDialog(true);
  }

  closeDialog(edited) {
    this.bsModalRef.hide();
  }
}