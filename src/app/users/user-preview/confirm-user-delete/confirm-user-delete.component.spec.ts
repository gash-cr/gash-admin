import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { ConfirmUserDeleteComponent } from './confirm-user-delete.component'
import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'
import { HttpClientModule } from '@angular/common/http'
import { ModalModule, BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import { ToastrModule } from 'ngx-toastr'

describe('ConfirmUserDeleteComponent', () => {
  let component: ConfirmUserDeleteComponent;
  let fixture: ComponentFixture<ConfirmUserDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConfirmUserDeleteComponent],
      imports: [ModalModule.forRoot(), HttpClientModule, ToastrModule.forRoot()],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
      providers: [BsModalRef, BsModalService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmUserDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
