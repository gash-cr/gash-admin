import { Component } from '@angular/core';
import { UserService } from '../../user-service/user.service';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-confirm-user-delete',
  templateUrl: './confirm-user-delete.component.html',
  styleUrls: ['../../../../theme/styles/user-styles.less']
})

export class ConfirmUserDeleteComponent {
  user: any;

  constructor(
    private userService: UserService,
    private toastr: ToastrService,
    public bsModalRef: BsModalRef
  ) { }

  deleteUser(): void {
    this.userService.delete(this.user._id);
    this.toastr.success('El usuario ha sido eliminado correctamente', 'Usuario eliminado');
    this.closeDialog();
  }

  closeDialog(): any {
    this.bsModalRef.hide();
  }
}
