import { Component } from '@angular/core'
import { UserEditModalComponent } from './user-edit-modal/user-edit-modal.component'
import { UserService } from '../user-service/user.service'
import { ToastrService } from 'ngx-toastr'
import { ConfirmUserDeleteComponent } from './confirm-user-delete/confirm-user-delete.component'
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal'
import { defaultUser } from '../../../assets/const/constants'

@Component({
  selector: 'app-user-preview',
  templateUrl: './user-preview.component.html',
  styleUrls: ['../../../theme/styles/user-styles.less']
})

export class UserPreviewComponent {
  user;
  editModal;
  deleteModal;

  constructor(private userService: UserService,
    private toastr: ToastrService,
    public bsModalRef: BsModalRef,
    private modalService: BsModalService
  ) {
    this.editModal = UserEditModalComponent;
    this.deleteModal = ConfirmUserDeleteComponent;
  }

  showEditUserModal(user) {
    user = (user) ? user : defaultUser;
    const initialState = { user: user };
    this.modalService.show(this.editModal, { initialState });
  }

  showDeleteUserConfirmModal(user) {
    const initialState = { user: user };
    this.modalService.show(this.deleteModal, { initialState });
    this.modalService.onHide.subscribe(() => {
      this.closeDialog();
    });
  }

  closeDialog() {
    this.bsModalRef.hide();
  }
}
