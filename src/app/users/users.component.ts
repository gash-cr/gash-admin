import { Component, ViewChild } from '@angular/core';
import { UserModalComponent } from '../users/user-modal/user-modal.component';
import { AdminModalComponent } from '../users/admin-modal/admin-modal.component';
import { MatTableDataSource, MatSort } from '@angular/material';
import { UserService } from './user-service/user.service';
import { UserPreviewComponent } from '../users/user-preview/user-preview.component';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-users',
  templateUrl: 'users.component.html',
  styleUrls: ['./users.component.less']
})

export class UsersComponent {
  userDialog: any;
  userComponent: any;
  registeredUsers: any;
  displayedColumns: string[] = ['name', 'username'];
  dataSource: any;
  userModalRef: BsModalRef;

  constructor(private userService: UserService, private modalService: BsModalService) {
    this.getUsersFromDB();
  }

  @ViewChild(MatSort) sort: MatSort;

  getUsersFromDB() {
    this.userService.get().subscribe(data => {
      this.registeredUsers = data;
      this.dataSource = new MatTableDataSource(this.registeredUsers);
      this.dataSource.sort = this.sort;
    });
  }

  showCreateUserModal(userType: string) {
    if (userType.match('admin')) {
      this.userComponent = AdminModalComponent;
    } else {
      this.userComponent = UserModalComponent;
    }

    this.userModalRef = this.modalService.show(this.userComponent);
    this.modalService.onHide.subscribe(() => {
      this.getUsersFromDB();
    });
  }

  showUserPreviewModal(user) {
    const initialState = { user: user };
    this.modalService.show(UserPreviewComponent, { initialState });
    this.modalService.onHide.subscribe(() => {
      this.getUsersFromDB();
    });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLocaleLowerCase();
  }
}