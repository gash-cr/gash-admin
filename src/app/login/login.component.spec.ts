import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { LoginComponent } from './login.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RouterTestingModule } from '@angular/router/testing'
import { AuthGuard } from '../authGuard/auth-guard'
import { NavComponent } from '../nav/nav.component'
import { LockerModule } from 'angular-safeguard'
import { StorageServiceModule } from 'angular-webstorage-service'
import { HttpClientModule } from '@angular/common/http'
import { ToastrModule } from 'ngx-toastr'
import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent, NavComponent],
      imports: [FormsModule, ReactiveFormsModule, RouterTestingModule, LockerModule, StorageServiceModule, HttpClientModule, ToastrModule.forRoot()],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
      providers: [AuthGuard]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
