import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Locker, DRIVERS } from 'angular-safeguard';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthGuard } from '../authGuard/auth-guard';
import { User } from '../models/User';
import { AuthService } from '../authGuard/auth.service';
import { ToastrService } from 'ngx-toastr';
import { StorageService, SESSION_STORAGE } from 'angular-webstorage-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})

export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private guard: AuthGuard,
              private authService: AuthService,
              private toastr: ToastrService,
              @Inject(SESSION_STORAGE) private storage: StorageService) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  // convenience getter for easy access to form fields
  get form() { return this.loginForm.controls; }

  onSubmit() {
    const user: User = { username: this.form.username.value, password: this.form.password.value };
    this.submitted = true;

    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;

    this.authService.login(user)
      .subscribe(res => this.authSuccess(res),
        err => this.authError(err));
  }

  authSuccess(res) {
    if (res.isAdmin) {
      this.loading = false;
      this.guard.setSession(res.token);
      this.toastr.clear();
      this.storage.set('shipper', res.shippingCompany);
      this.router.navigate(['/revision']);
    } else {
      this.loading = false;
      this.toastr.error('Usuario no es un administrador', 'Acceso denegado');
    }
  }

  authError(err) {
    this.loading = false;
    this.toastr.error('Usuario o contraseña incorrectos', 'Autentificación Incorrecta');
  }
}