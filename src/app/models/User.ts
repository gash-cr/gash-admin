export interface User {
  createdAt?: Date;
  updatedAt?: Date;
  username: string;
  password: string;
  name?: string;
  isAdmin?: boolean;
  shippingCompany?: string;
}
