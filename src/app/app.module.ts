import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AppRoutingModule, routedComponents } from './app-routing.module';
import { LockerModule } from 'angular-safeguard';
import { AuthGuard } from './authGuard/auth-guard';
import { NavComponent } from './nav/nav.component';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './authGuard/auth.service';
import { UserService } from './users/user-service/user.service';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { RevisionsListComponent } from './revisions/revisions-list/revisions-list.component';
import { MatGridListModule, MatFormFieldModule, MatTableModule, MatSortModule, MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule } from '@angular/material';
import { ShowRevisionComponent } from './revisions/show-revision/show-revision.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { UsersComponent } from './users/users.component';
import { UserModalComponent } from './users/user-modal/user-modal.component';
import { AdminModalComponent } from './users/admin-modal/admin-modal.component';
import { UserPreviewComponent } from './users/user-preview/user-preview.component';
import { UserEditModalComponent } from './users/user-preview/user-edit-modal/user-edit-modal.component';
import { ConfirmUserDeleteComponent } from './users/user-preview/confirm-user-delete/confirm-user-delete.component';
import { StorageServiceModule } from 'angular-webstorage-service';
import { RevisionDamagesComponent } from './revisions/revision-damages/revision-damages.component';
import { ReportComponent } from './revisions/report/report.component';
import { RoleGuard } from './authGuard/role-guard';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatPaginatorIntl } from '@angular/material';
import { PaginatorIntlService } from './revisions/paginator-intl.service';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    LockerModule,
    HttpClientModule,
    AngularFontAwesomeModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    AngularFontAwesomeModule,
    MatFormFieldModule,
    MatSortModule,
    ModalModule.forRoot(),
    MatDialogModule,
    MatTableModule,
    StorageServiceModule,
    MatGridListModule,
    MatPaginatorModule,
  ],
  declarations: [
    AppComponent,
    routedComponents,
    NavComponent,
    RevisionsListComponent,
    ShowRevisionComponent,
    UsersComponent,
    UserModalComponent,
    AdminModalComponent,
    UserPreviewComponent,
    UserEditModalComponent,
    ConfirmUserDeleteComponent,
    RevisionDamagesComponent,
    ReportComponent,
  ],
  providers: [
    AuthGuard,
    RoleGuard,
    AuthService,
    UserService,
    { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: false } },
    { provide: MatPaginatorIntl, useClass: PaginatorIntlService },
  ],
  bootstrap: [AppComponent],
  entryComponents: [AdminModalComponent, UserModalComponent, UserPreviewComponent, UserEditModalComponent, ConfirmUserDeleteComponent, ShowRevisionComponent]
})

export class AppModule { }