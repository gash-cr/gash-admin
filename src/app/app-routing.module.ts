import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './authGuard/auth-guard';
import { RevisionsListComponent } from './revisions/revisions-list/revisions-list.component';
import { ShowRevisionComponent } from './revisions/show-revision/show-revision.component';
import { UsersComponent } from './users/users.component';
import { RevisionDamagesComponent } from './revisions/revision-damages/revision-damages.component';
import { RoleGuard } from './authGuard/role-guard';

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'revision-damages', component: RevisionDamagesComponent, canActivate: [AuthGuard] },
  { path: 'revision', component: RevisionsListComponent, canActivate: [AuthGuard] },
  { path: 'revision-preview', component: ShowRevisionComponent, canActivate: [AuthGuard] },
  { path: 'users', component: UsersComponent, canActivate: [RoleGuard], data: { allowedRoles: ['Gash'], redirectTo: '/revision', key: 'shipper' } },
  { path: '**', redirectTo: '' } // Redirects unknown links to /login
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
export const routedComponents = [HomeComponent, LoginComponent, HomeComponent, UsersComponent, RevisionDamagesComponent];
