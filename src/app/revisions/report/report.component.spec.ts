import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { ReportComponent } from './report.component'
import { StorageServiceModule } from 'angular-webstorage-service'

describe('ReportComponent', () => {
  let component: ReportComponent;
  let fixture: ComponentFixture<ReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReportComponent],
      imports: [StorageServiceModule],
      providers: [StorageServiceModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    fixture.detectChanges()
    expect(component).toBeTruthy();
  });
});
