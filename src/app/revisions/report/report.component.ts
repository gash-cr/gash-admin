import { Component, OnInit, Inject } from '@angular/core'
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service'
import { getShipperParts, GENERAL_INFO, REVISION_STORAGE_KEY, defaultRevision, EVERGREEN_GENERAL_INFO } from '../../../assets/const/constants'
import * as jsPDF from 'jspdf'

@Component({
  selector: 'app-report',
  template: '<button class="btn btn-danger" (click)="generateReport()">Reporte</button>',
})

export class ReportComponent implements OnInit {
  revision;
  imagesPath = '../../../assets/images/';
  damagedParts = [];
  drawableDamagedParts = [];
  differentSizedImages = ['chassis_back', 'chassis_front', 'chassis_side', 'chassis_up', 'floor', 'roof'];
  revisionShipper;
  isReportAvailable = false;
  generalInfo;
  gashDamages;
  gashParts;
  evergreenDamages;

  constructor(@Inject(SESSION_STORAGE) private storage: StorageService) {
    this.revision = this.storage.get(REVISION_STORAGE_KEY) || defaultRevision;
    this.damagedParts = getShipperParts(this.revision.shipperName);
  }

  ngOnInit(): void {
    this.revisionShipper = this.revision.shipperName.toLowerCase();
    if (this.revisionShipper === 'gash') {
      this.loadImage(`${this.revisionShipper}_general_info`).then(image => {
        this.generalInfo = this.encodeBase64Image(image);
      });
      this.loadImage(`${this.revisionShipper}_damages_section`).then(image => {
        this.gashDamages = this.encodeBase64Image(image);
      });
    } else if (this.revisionShipper === 'nyk'){
      this.loadImage(`${this.revisionShipper}_general_info`).then(image => {
        this.generalInfo = this.encodeBase64Image(image);
      });
    } else{
      this.loadImage(`evergreen_general_info4.3`).then(image => {
        this.generalInfo = this.encodeBase64Image(image);
      });
    }
  }

  ngAfterViewInit(): void {
    this.createDamages()
      .then(() => this.drawDamagesAbbreviations())
      .then(() => { this.isReportAvailable = true })
  }

  loadImage(src) {
    return new Promise((resolve, reject) => {
      let img = new Image()
      img.onload = () => resolve(img);
      img.onerror = reject
      img.src = `${this.imagesPath}report/${src}.png`
    })
  }

  encodeBase64Image(img) {
    const canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    const ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    return canvas.toDataURL("image/png");
  }

  createDamages(): Promise<any> {
    const damagesToDraw: Array<any> = [];
    this.damagedParts.forEach((part) => {
      damagesToDraw.push(new Promise((resolve, reject) => {
        let canvas = document.createElement('canvas');
        canvas.setAttribute('id', part.id);
        let background = new Image();
        let damagesFolder = (this.differentSizedImages.includes(part.id)) ? `no_margin_damages` : 'damages';
        background.src = `../../../assets/images/${damagesFolder}/${part.id}.png`;
        background.onload = () => {
          canvas.width = background.width+210;
          canvas.height = background.height+210;
          let context = canvas.getContext('2d');
          context.fillStyle = '#ffffff';
          context.fillRect(0, 0, canvas.width, canvas.height);
          context.drawImage(background, 0, 0, background.width, background.height);
        };
        this.drawableDamagedParts.push(canvas);
        resolve();
      }));
    });
    return Promise.all(damagesToDraw);
  }

  findCanvasElement(canvasId) {
    return this.drawableDamagedParts.filter((canvas) => canvas.id === canvasId).pop();
  }

  drawDamagesAbbreviations(): Promise<any> {
    const abbreviationsToDraw = [];
    if (this.revision.damages && this.revision.damages !== [] && this.revision.damages !== undefined) {
      this.revision.damages.forEach((damagedPart) => {
        abbreviationsToDraw.push(new Promise((resolve, reject) => {
          let canvas = this.findCanvasElement(damagedPart.damagedPartId);
          let context = canvas.getContext('2d');
          context.font = '45px Arial';
          context.fillStyle = 'red';
          context.fillText(damagedPart.damage.abbreviation, damagedPart.coordinateX, damagedPart.coordinateY);
          resolve();
        }));
      });
    }
    return Promise.all(abbreviationsToDraw);
  }

  printGeneralInfo(pdfDocument, revision) {
    let createdAt = new Date(this.revision.createdAt);
    let info;
    if(this.revisionShipper === "gash" || this.revisionShipper === "nyk") {
      info = GENERAL_INFO;
    } else {
      info = EVERGREEN_GENERAL_INFO
    }
    info.forEach(revisionData => {
      let dataToPrint
      if(revisionData.directive == "revision.genSet"){
        dataToPrint = eval(revisionData.directive).toString();
      } else{
        dataToPrint = eval(revisionData.directive);
      }
      if(revisionData.directive == "revision.genSet"){
        dataToPrint = (dataToPrint) ? 'SI' : 'NO'
      }
      dataToPrint = (!dataToPrint || dataToPrint === '' || dataToPrint === undefined) ? 'N/A' : dataToPrint;
      pdfDocument.text(dataToPrint, revisionData.coordinateX, revisionData.coordinateY);
    });
    return pdfDocument;
  }

  printMarks(marks, pdfDocument, coordinateY) {
    this.revisionShipper = this.revision.shipperName.toLowerCase();
    if(this.revisionShipper === "gash"){
      if (marks && marks !== undefined) {
          let mark = (!marks[0] || marks[0] === '') ? 'N/A' : marks[0];
          pdfDocument.text(mark, 18, coordinateY);

          mark = (!marks[1] || marks[1] === '') ? 'N/A' : marks[1];
          pdfDocument.text(mark, 46, coordinateY);

          mark = (!marks[2] || marks[2] === '') ? 'N/A' : marks[2];
          pdfDocument.text(mark, 79, coordinateY);

          mark = (!marks[3] || marks[3] === '') ? 'N/A' : marks[3];
          pdfDocument.text(mark, 110, coordinateY);

          mark = (!marks[4] || marks[4] === '') ? 'N/A' : marks[4];
          pdfDocument.text(mark, 140, coordinateY);

          mark = (!marks[5] || marks[5] === '') ? 'N/A' : marks[5];
          pdfDocument.text(mark, 170, coordinateY);
      }
    } else if(this.revisionShipper === "evergreen"){
        if (marks && marks !== undefined) {
          let mark = (!marks[0] || marks[0] === '') ? 'N/A' : marks[0];
          pdfDocument.text(mark, 92, coordinateY);

          mark = (!marks[1] || marks[1] === '') ? 'N/A' : marks[1];
          pdfDocument.text(mark, 112, coordinateY);

          mark = (!marks[2] || marks[2] === '') ? 'N/A' : marks[2];
          pdfDocument.text(mark, 133, coordinateY);

          mark = (!marks[3] || marks[3] === '') ? 'N/A' : marks[3];
          pdfDocument.text(mark, 149, coordinateY);

          mark = (!marks[4] || marks[4] === '') ? 'N/A' : marks[4];
          pdfDocument.text(mark, 167, coordinateY);

          mark = (!marks[5] || marks[5] === '') ? 'N/A' : marks[5];
          pdfDocument.text(mark, 187, coordinateY);
      }
    } else if(this.revisionShipper === "nyk"){
      if (marks && marks !== undefined) {
        let mark = (!marks[0] || marks[0] === '') ? 'N/A' : marks[0];
        pdfDocument.text(mark, 18, coordinateY);

        mark = (!marks[1] || marks[1] === '') ? 'N/A' : marks[1];
        pdfDocument.text(mark, 46, coordinateY);

        mark = (!marks[2] || marks[2] === '') ? 'N/A' : marks[2];
        pdfDocument.text(mark, 79, coordinateY);

        mark = (!marks[3] || marks[3] === '') ? 'N/A' : marks[3];
        pdfDocument.text(mark, 110, coordinateY);

        mark = (!marks[4] || marks[4] === '') ? 'N/A' : marks[4];
        pdfDocument.text(mark, 140, coordinateY);

        mark = (!marks[5] || marks[5] === '') ? 'N/A' : marks[5];
        pdfDocument.text(mark, 170, coordinateY);
    }
  }
    return pdfDocument;
  }

  printDamages(pdf) {
    if (this.drawableDamagedParts && this.drawableDamagedParts !== undefined) {
      this.drawableDamagedParts.forEach((damage, index) => {
        const part = this.damagedParts[index];
        pdf.addImage(damage, part.xCoord, part.yCoord, part.width, part.height);
      });
    }
    return pdf;
  }

  printGashParts(parts, pdf) {
    if (parts !== undefined && parts) {
      let yPosition = 138;
      parts.forEach(part => {
        pdf.text(part.name, 60, yPosition);
        pdf.text(this.isPart(part.in), 115, yPosition);
        pdf.text(this.isPart(part.out), 138, yPosition)
        yPosition += 8;
      });
    }
    return pdf;
  }

  isPart(part) {
    return (part) ? 'SI' : 'NO';
  }

  public generateReport() {
    if (this.isReportAvailable) {
      let pdf = new jsPDF('p', 'mm', 'a4');
      let createdAt = new Date(this.revision.createdAt);
      pdf.setFont('Arial');
      pdf.setFontSize('12');
      pdf.addImage(this.generalInfo, 0, 0, pdf.internal.pageSize.getWidth(), pdf.internal.pageSize.getHeight());
      pdf = this.printGeneralInfo(pdf, this.revision);
      console.log(this.revision);
      if (this.revisionShipper === 'gash') {
        pdf = this.printMarks(this.revision.tires, pdf, 73);
        pdf = this.printMarks(this.revision.marks, pdf, 91);
        this.printGashParts(this.revision.part, pdf);
        pdf.addPage();
        pdf.addImage(this.gashDamages, 0, 0, pdf.internal.pageSize.getWidth(), pdf.internal.pageSize.getHeight());
        pdf.addImage(this.gashDamages, 0, 0, pdf.internal.pageSize.getWidth(), pdf.internal.pageSize.getHeight());
        this.drawDamagesAbbreviations();
        pdf = this.printDamages(pdf);
      }else if (this.revisionShipper === 'evergreen'){
        this.drawDamagesAbbreviations();
        pdf = this.printDamages(pdf);
        pdf.setFontSize('9');
        pdf = this.printMarks(this.revision.tires, pdf, 253);
        pdf = this.printMarks(this.revision.marks, pdf, 269);
      } else if (this.revisionShipper === 'nyk') {
        this.drawDamagesAbbreviations();
        pdf = this.printDamages(pdf);
        pdf = this.printMarks(this.revision.tires, pdf, 73);
        pdf = this.printMarks(this.revision.marks, pdf, 91);
      }
      pdf.save(`TIR # ${this.revision.containerCode} - ${createdAt.toLocaleDateString("en-GB")}  ${createdAt.toLocaleTimeString()}`);
    }
  }
}

