import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { NavComponent } from '../../nav/nav.component'
import { RevisionDamagesComponent } from './revision-damages.component'
import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'
import { StorageServiceModule } from 'angular-webstorage-service'
import { RouterTestingModule } from '@angular/router/testing'
import { LockerModule } from 'angular-safeguard'

describe('RevisionDamagesComponent', () => {
  let component: RevisionDamagesComponent;
  let fixture: ComponentFixture<RevisionDamagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RevisionDamagesComponent, NavComponent],
      imports: [StorageServiceModule, RouterTestingModule, LockerModule],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RevisionDamagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
