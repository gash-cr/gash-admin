import { Component, ViewChild, ElementRef, Renderer, AfterViewInit, Inject } from '@angular/core'
import { getShipperParts, REVISION_STORAGE_KEY, defaultRevision } from '../../../assets/const/constants'
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service'
import { Router } from '@angular/router'
@Component({
  selector: 'app-damages-revision',
  templateUrl: './revision-damages.component.html',
  styleUrls: ['./revision-damages.component.less']
})

export class RevisionDamagesComponent implements AfterViewInit {
  revision;
  parts;
  currentDamages = [];
  differentSizedImages = ['chassis_back', 'chassis_front', 'chassis_side', 'chassis_up', 'floor', 'roof'];
  damageLabel;
  canvasBackground = new Image();
  image_ref;
  imagesPath = '../../../assets/images/';

  @ViewChild('imageCanvas') canvas: ElementRef;
  canvasElement: any;
  context: any;

  constructor(public renderer: Renderer,
    @Inject(SESSION_STORAGE) private storage: StorageService,
    private router: Router) {
    this.revision = this.storage.get(REVISION_STORAGE_KEY) || defaultRevision;
    this.parts = getShipperParts(this.revision.shipperName);
  }

  ngAfterViewInit(): void {
    if (this.revision.damages && this.revision.damages !== []) {
      this.canvasElement = this.canvas.nativeElement;
      this.context = this.canvasElement.getContext('2d');
      let containerWithDamages = this.revision.damages.map(damagedContainer => damagedContainer.containerId);
      containerWithDamages = containerWithDamages.filter((item, index) => (containerWithDamages.indexOf(item) >= index));
      this.revision.damages.forEach(element => {
        this.addMarkToContainer(element.damagedPartId);
      });
    }
  }

  displayDamages(damagedPart) {
    this.context.clearRect(0, 0, this.canvasElement.width, this.canvasElement.height);
    this.currentDamages = this.revision.damages.filter(part => part.damagedPartId === damagedPart.id);
    this.damageLabel = damagedPart.name;
    let damagesFolder = 'damages'
    this.canvasElement.width = 370;
    this.canvasElement.height = 370;
    if (this.differentSizedImages.includes(damagedPart.id)) {
      this.canvasElement.height = 170;
      damagesFolder = `no_margin_damages`;
    };
    this.canvasBackground.src = `../../../assets/images/${damagesFolder}/${damagedPart.id}.png`;
    this.canvasBackground.onload = () => {
      this.context.drawImage(this.canvasBackground, 0, 0, this.canvasElement.width, this.canvasElement.height);
      this.currentDamages.forEach(element => {
        this.drawDamage(element.coordinateX / (this.canvasBackground.width / this.canvasElement.width), element.coordinateY / (this.canvasBackground.height / this.canvasElement.height), element.damage.abbreviation);
      });
    };
  };

  drawDamage(x: number, y: number, text: string): void {
    this.context.font = "25px Arial";
    this.context.fillStyle = "red";
    this.context.fillText(text, x, y);
  }

  addMarkToContainer(containerId) {
    document.getElementById(containerId).classList.add('damaged-container');
  }

  goToRevision() {
    this.router.navigate(['/revision-preview']);
  }

  setImage_ref(image_ref) {
    this.image_ref = `data:image/jpeg;base64,${image_ref}`;
  }
}