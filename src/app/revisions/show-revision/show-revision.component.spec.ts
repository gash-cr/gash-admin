import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { ShowRevisionComponent } from './show-revision.component'
import { NavComponent } from '../../nav/nav.component'
import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'
import { MatTableModule } from '@angular/material'
import { RouterTestingModule } from '@angular/router/testing'
import { StorageServiceModule } from 'angular-webstorage-service'
import { AuthGuard } from '../../authGuard/auth-guard'
import { LockerModule } from 'angular-safeguard'

describe('ShowRevisionComponent', () => {
  let component: ShowRevisionComponent;
  let fixture: ComponentFixture<ShowRevisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ShowRevisionComponent, NavComponent],
      imports: [MatTableModule, RouterTestingModule, StorageServiceModule, LockerModule],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
      providers: [AuthGuard]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowRevisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
