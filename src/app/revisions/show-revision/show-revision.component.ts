import { Component, OnInit } from '@angular/core'
import { MatTableDataSource } from '@angular/material'
import { Router } from '@angular/router'
import { Inject } from '@angular/core'
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service'
import { REVISION_STORAGE_KEY, defaultRevision } from '../../../assets/const/constants'

@Component({
  selector: 'app-show-revision',
  host: { 'window:beforeunload': 'doSomething' },
  templateUrl: './show-revision.component.html',
  styleUrls: ['./show-revision.component.less']
})

export class ShowRevisionComponent implements OnInit {
  dataSource: any;
  displayedColumns: string[] = ['parte', 'entrada', 'salida'];
  revision;
  user;

  constructor(private router: Router, @Inject(SESSION_STORAGE) private storage: StorageService) { }

  ngOnInit() {
    this.revision = this.storage.get(REVISION_STORAGE_KEY) || defaultRevision;
    this.dataSource = new MatTableDataSource(this.revision.part);
  }

  viewDamages() {
    this.router.navigate(['/revision-damages']);
  }
}