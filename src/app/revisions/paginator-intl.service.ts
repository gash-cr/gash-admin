import { MatPaginatorIntl } from "@angular/material";

export class PaginatorIntlService extends MatPaginatorIntl {
  constructor() {
    super();
    this.getAndInitTranslation();
  }

  getAndInitTranslation() {
    this.itemsPerPageLabel = 'Revisiones por página:';
    this.firstPageLabel = '';
    this.nextPageLabel = '';
    this.previousPageLabel = '';
    this.lastPageLabel = '';
    this.changes.next();
  }

  getRangeLabel = (page: number, pageSize: number, length: number) => {
    if (length === 0 || pageSize === 0) {
      return `0 / ${length}`;
    }

    length = Math.max(length, 0);
    const startIndex = page * pageSize;
    const endIndex = startIndex < pageSize ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;
    return `${startIndex + 1} - ${endIndex > length ? length : endIndex} de ${length}`;
  }
}