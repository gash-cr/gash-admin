import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class RevisionService {
  userUrl = environment.apiUrl + 'revision';

  constructor(private http: HttpClient) { }

  public getRevisions(pageNumber: number) {
    return this.http.get(`${this.userUrl}?pageNo=${pageNumber}&size=10`);
  }

  public getRevisionsByShipper(shipper) {
    return this.http.get(`${this.userUrl}/by/${shipper}`);
  }

  public getRevisionsBy(condition: string, pageNumber: number) {
    return this.http.get(`${this.userUrl}/search?condition=${condition}&pageNo=${pageNumber}&size=10`);
  }

  public delete(id) {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      body: { _id: id }
    };

    return this.http.delete(this.userUrl, httpOptions).subscribe();
  }
}
