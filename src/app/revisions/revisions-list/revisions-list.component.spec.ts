import { async, ComponentFixture, TestBed } from '@angular/core/testing'
import { RevisionsListComponent } from './revisions-list.component'
import { NavComponent } from '../../nav/nav.component'
import { NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core'
import { MatTableModule } from '@angular/material'
import { HttpClientModule } from '@angular/common/http'
import { RouterTestingModule } from '@angular/router/testing'
import { ToastrModule } from 'ngx-toastr'
import { StorageServiceModule } from 'angular-webstorage-service'
import { AuthGuard } from '../../authGuard/auth-guard'
import { LockerModule } from 'angular-safeguard'

describe('RevisionsListComponent', () => {
  let component: RevisionsListComponent;
  let fixture: ComponentFixture<RevisionsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RevisionsListComponent, NavComponent],
      imports: [MatTableModule, HttpClientModule, RouterTestingModule, ToastrModule.forRoot(), StorageServiceModule, LockerModule],
      schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA],
      providers: [AuthGuard]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RevisionsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
