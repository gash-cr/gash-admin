import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { PageEvent } from '@angular/material/paginator';
import { Router, ActivatedRoute } from '@angular/router';
import { RevisionService } from '../revision.service';
import { ToastrService } from 'ngx-toastr';
import { Inject } from '@angular/core';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';
import { REVISION_STORAGE_KEY } from '../../../assets/const/constants';

@Component({
  selector: 'app-revisions-list',
  templateUrl: './revisions-list.component.html',
  styleUrls: ['./revisions-list-component.less']
})

export class RevisionsListComponent implements OnInit {
  revisions: any;
  dataSource: any;
  pageSize: number;
  length: number;
  searchCondition: string = '';
  displayedColumns: string[] = ['container', 'chassis', 'truck', 'date'];

  constructor(private revisionService: RevisionService,
              private router: Router,
              private toastr: ToastrService,
              @Inject(SESSION_STORAGE) private storage: StorageService,
              private route: ActivatedRoute) {
  }

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.dataSource = new MatTableDataSource(this.revisions);
    this.dataSource.paginator = this.paginator;
    this.getRevisionsList(this.storage.get('shipper'));
  }

  getRevisions() {
    this.revisionService.getRevisions(this.paginator.pageIndex++)
      .subscribe((revisions: any) => {
        this.revisions = revisions.docs;
        this.length = revisions.totalDocs;
        this.dataSource = new MatTableDataSource(this.revisions);
        this.dataSource.paginator = this.paginator;
      }, () => this.toastr.error('No se pudo recuperar las revisiones de la base de datos', 'Error'));
  }

  getRevisionsByShipper(shipper) {
    this.revisionService.getRevisionsByShipper(shipper)
      .subscribe((revisions: any) => {
        this.revisions = revisions.docs;
        this.length = revisions.totalDocs;
        this.dataSource = new MatTableDataSource(this.revisions);
        this.dataSource.paginator = this.paginator;
      }, () => this.toastr.error('No se pudo recuperar las revisiones de la base de datos', 'Error'))
  }

  onPaginateChange(event?: PageEvent, condition?: string) {
    let page = event ? event.pageIndex++ : 1;
    if (condition != '' && event != null) {
      this.revisionService.getRevisionsBy(condition, page).subscribe((revisions: any) => {
        this.revisions = revisions.docs;
        this.length = revisions.totalDocs;
        this.dataSource = new MatTableDataSource(this.revisions);
      }, () => this.toastr.error('No se pudo recuperar las revisiones de la base de datos', 'Error'));
    } else {
      this.getRevisionsByPage(page)
    }
  }

  getRevisionsByPage(page: number) {
    this.revisionService.getRevisions(page).subscribe((revisions: any) => {
      this.revisions = revisions.docs;
      this.length = revisions.totalDocs;
      this.dataSource = new MatTableDataSource(this.revisions);
    }, () => this.toastr.error('No se pudo recuperar las revisiones de la base de datos', 'Error'));
  }

  getRevisionsList(shipper) {
    return (shipper === 'Gash') ? this.getRevisions() : this.getRevisionsByShipper(shipper);
  }

  search(condition: string) {
    this.revisionService.getRevisionsBy(condition, 1).subscribe((revisions: any) => {
      this.revisions = revisions.docs;
      this.length = revisions.totalDocs;
      this.dataSource = new MatTableDataSource(this.revisions);
    }, () => this.toastr.error('No se pudo recuperar las revisiones de la base de datos', 'Error'));
  }

  clear() {
    this.searchCondition = '';
    this.getRevisionsByPage(1);
  }

  viewRevision(revision) {
    this.storage.set(REVISION_STORAGE_KEY, revision);
    this.router.navigate(['/revision-preview']);
  }
}
