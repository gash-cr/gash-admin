(function() {
  'use strict';

  angular
    .module('app', [
      'app.core',
      'app.layout',
      'app.login',
      'app.home',
      'app.users',
      'app.revisions',
      'ngMessages'
    ]);

})();
