(function() {
  'use strict';

  angular
    .module('app.users')
    .controller('ShowUserController', ShowUserController);

  /* @ngInject */
  function ShowUserController(usersService,
                              userId,
                              $state,
                              $uibModal,
                              $uibModalInstance) {
    var vm = this;

    vm.deleteUser = deleteUser;
    vm.editUser = editUser;
    vm.cancelUser = cancelUser;
    vm.loadUser = loadUser;
    vm.userId = userId;

    activate();

    function activate() {
      usersService.getUser(vm.userId)
      .then(loadUser);
    }

    function loadUser(user) {
      vm.user = user;
    }

    function deleteUser() {
      $uibModalInstance.dismiss('cancel');
      $uibModal.open({
        templateUrl: 'app/components/users/delete/user.delete.html',
        controller: 'DeleteUserController as vm',
        resolve: {
          userId: function() {
            return vm.userId;
          }
        }
      });
    }

    function editUser() {
      $uibModal.open({
        templateUrl: 'app/components/users/edit/user.edit.html',
        controller: 'EditUserController as vm',
        resolve: {
          userId: function() {
            return vm.userId;
          }
        }
      });
    }

    function cancelUser() {
      $uibModalInstance.dismiss('cancel');
    }

  }
})();
