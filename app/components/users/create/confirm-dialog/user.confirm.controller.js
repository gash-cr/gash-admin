(function() {
  'use strict';

  angular
    .module('app.users')
    .controller('ConfirmUserController', ConfirmUserController);

  /* @ngInject */
  function ConfirmUserController(usersService,
                                 $state,
                                 $uibModalInstance,
                                 $uibModalStack,
                                 user) {

    var vm = this;

    vm.createUser = createUser;
    vm.cancel = cancel;

    function createUser() {
      usersService.createUser(user)
      .then(goToUsers);
    }

    function goToUsers() {
      $uibModalStack.dismissAll();
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

})();
