(function() {
  'use strict';

  angular
    .module('app.users')
    .controller('createUserController', createUserController);

  /* @ngInject */
  function createUserController(usersService,
                                toastr,
                                $state,
                                $uibModal,
                                $uibModalInstance) {
    var vm = this;
    vm.createUser = createUser;
    vm.confirmUser = confirmUser;
    vm.redirectToUsersList = redirectToUsersList;
    vm.user = {};

    function confirmUser() {
      usersService.getUser(vm.user.username)
      .then(createUser);
    }

    function createUser(user) {
      if (angular.isDefined(user.username)) {
        toastr.error('Parece que tuvimos problemas al realizar la operación','Usuario no puede ser creado');
      } else {
        showConfirmUser();
      }
    }

    function showConfirmUser() {
      $uibModal.open({
        templateUrl: 'app/components/users/create/confirm-dialog/user.confirm.html',
        controller: 'ConfirmUserController as vm',
        resolve: {
          user: function() {
            return vm.user;
          }
        }
      });
    }

    function redirectToUsersList() {
      $uibModalInstance.dismiss('cancel');
    }

  }
})();
