(function() {
  'use strict';

  angular
    .module('app.users')
    .controller('EditUserController', EditUserController);

  /* @ngInject */
  function EditUserController(usersService,
                              $uibModalInstance,
                              $uibModalStack,
                              userId) {

    var vm = this;
    vm.loadUser = loadUser;
    vm.showUser = showUser;
    vm.backToUser = backToUser;
    vm.confirmUser = confirmUser;
    vm.userId = userId;

    activate();

    function activate() {
      vm.userExists = false;
      vm.userUsername = null;
      usersService.getUser(vm.userId)
      .then(loadUser);
    }

    function loadUser(user) {
      vm.user = user;
    }

    function showUser(user) {
      $uibModalInstance.dismiss('cancel');
    }

    function backToUser() {
      $uibModalStack.dismissAll();
    }

    function confirmUser() {
      if (vm.user.username == vm.userId) {
        vm.userUsername = vm.user.username;
        usersService.updateUser(vm.user)
        .then(showUser);
      } else {
        usersService.getUser(vm.user.username)
        .then(saveUser);
      }
    }

    function saveUser(user) {
      if (angular.isDefined(user.username)) {
        vm.userExists = true;
      } else {
        vm.userUsername = vm.user.username;
        usersService.editUser(vm.user)
        .then(deleteUser);
      }
    }

    function deleteUser() {
      usersService.deleteUser(vm.userId)
      .then(backToUser);
    }

  }
})();
