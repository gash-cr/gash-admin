(function() {
  'use strict';

  angular
    .module('app.users')
    .controller('UsersController', UsersController);

  /* @ngInject */
  function UsersController(usersService,
                           $state,
                           $uibModal) {
    var vm = this;
    vm.sort = {
      property: 'username',
      reverse: false
    };
    vm.loading = true;
    vm.sortBy = sortBy;
    vm.getUser = getUser;
    vm.showUser = showUser;
    vm.goToCreateUser = goToCreateUser;

    activate();

    function activate() {
      usersService.getUsers()
        .then(loadUsers);
    }

    function goToCreateUser() {
      $uibModal.open({
        templateUrl: 'app/components/users/create/user.create.html',
        controller: 'createUserController as vm'
      });
    }

    function loadUsers(users) {
      vm.users = users;
      vm.loading = false;
    }

    function sortBy(propertyName) {
      vm.sort.reverse = (vm.sort.property === propertyName) ? !vm.sort.reverse : false;
      vm.sort.property = propertyName;
    }

    function showUser(user) {
      $uibModal.open({
        templateUrl: 'app/components/users/show/user.show.html',
        controller: 'ShowUserController as vm',
        resolve: {
          userId: function() {
            return user.$id;
          }
        }
      });
    }

    function getUser(user) {
      $state.go('home.showUser', {userId: user.$id});
    }
  }

})();
