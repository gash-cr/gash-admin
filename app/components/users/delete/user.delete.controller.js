(function() {
  'use strict';

  angular
    .module('app.users')
    .controller('DeleteUserController', DeleteUserController);

  /* @ngInject */
  function DeleteUserController(usersService,
                                $state,
                                $uibModal,
                                $uibModalInstance,
                                $uibModalStack,
                                userId) {
    var vm = this;

    vm.deleteUser = deleteUser;
    vm.goToUsers = goToUsers;
    vm.cancel = cancel;

    function deleteUser() {
      usersService.deleteUser(userId)
      .then(goToUsers);
    }

    function goToUsers() {
      $uibModalStack.dismissAll();
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
      $uibModal.open({
        templateUrl: 'app/components/users/show/user.show.html',
        controller: 'ShowUserController as vm',
        resolve: {
          userId: function() {
            return userId;
          }
        }
      });
    }
  }

})();
