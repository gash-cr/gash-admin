(function() {
  'use strict';

  angular
    .module('app.users')
    .factory('usersService', usersService);

  /* @ngInject */
  function usersService($firebaseArray, $firebaseObject) {
    var rootRef = firebase.database().ref();
    var service = {
      getUsers: getUsers,
      getUser: getUser,
      deleteUser: deleteUser,
      editUser: editUser,
      createUser: createUser,
      updateUser: updateUser
    };

    return service;

    function getUsers() {
      var userRef = rootRef.child('users');
      return $firebaseArray(userRef).$loaded();
    }

    function getUser(userNumber) {
      var userRef = rootRef.child('users').child(userNumber);
      return $firebaseObject(userRef).$loaded();
    }

    function deleteUser(userNumber) {
      var userRef = rootRef.child('users').child(userNumber);
      return $firebaseObject(userRef).$remove();
    }

    function editUser(user) {
      var userUser = user.username;
      var userNewData = {name: user.name, username: user.username, password: user.password};
      return rootRef.child('users').child(userUser).update(userNewData);
    }

    function updateUser(user) {
      var userId = user.$id;
      var userRef = rootRef.child('users').child(userId);
      var userNewData = {name: user.name, password: user.password};
      return userRef.update(userNewData);
    }

    function createUser(userData) {
      var userRef = rootRef.child('users').child(userData.username);
      return userRef.set(userData);
    }
  }
})();
