(function() {
  'use strict';

  angular
    .module('app.users')
    .config(routesConfiguration);

  /* @ngInject */
  function routesConfiguration($stateProvider) {
    $stateProvider
      .state('home.users', {
        url: '/users',
        templateUrl: 'app/components/users/users.html',
        controller: 'UsersController as vm'
      })
      .state('home.showUser', {
        url: '/user/:userId',
        templateUrl: 'app/components/users/show/user.show.html',
        controller: 'ShowUserController as vm'
      })
      .state('home.editUser', {
        url: '/edit-user/:userId',
        templateUrl: 'app/components/users/edit/user.edit.html',
        controller: 'EditUserController as vm'
      })
      .state('home.userCreate', {
        url: '/user-create',
        templateUrl: 'app/components/users/create/user.create.html',
        controller: 'createUserController as vm'
      });
  }
})();
