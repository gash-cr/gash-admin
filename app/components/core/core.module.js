(function() {
  'use strict';

  function initAuth(loginService, $rootScope) {
    $rootScope.logOut = loginService.logOut;
    $rootScope.isLoggedIn = loginService.isLoggedIn;
    loginService.verifyAccess();
  }

  angular
    .module('app.core', [
      'firebase',
      'ui.router',
      'toastr',
      'ngMessages'
    ])
    .run(initAuth);
})();
