(function() {
  'use strict';

  // Uncomment the path of the PDF generator web service
  var STAGING_API_PATH    = 'https://gash-api-staging.herokuapp.com/api/v1/';
  //var PRODUCTION_API_PATH = 'https://gash-api-prod.herokuapp.com/api/v1/';
  //var LOCAL_PATH          = 'http://localhost:8080/api/v1/';

  angular
    .module('app')
    .constant('CORE', {
      'API_URL': STAGING_API_PATH
    });
})();
