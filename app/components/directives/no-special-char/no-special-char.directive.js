(function() {
  'use strict';

  angular
    .module('app')
    .directive('noSpecialChar', noSpecialChar);

  function noSpecialChar() {

    var directive = {
      require: 'ngModel',
      restrict: 'A',
      link: function(scope, element, attr, userCtrl) {
        userCtrl.$parsers.push(function(inputValue) {
          if (inputValue == null) {
            return '';
          }
          var cleanInputValue = inputValue.replace(/[^\w\s]/gi, '');
          if (cleanInputValue != inputValue) {
            userCtrl.$setViewValue(cleanInputValue);
            userCtrl.$render();
          }
          return cleanInputValue;
        });
      }
    };
    return directive;
  }

})();
