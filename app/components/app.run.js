(function() {
  'use strict';

  angular
    .module('app')
    .run(run);

  /* @ngInject */
  function run($window) {
    var devConfig = {
      apiKey: 'AIzaSyB6NIRIix6d20vqoNnuGePTYVTgNqo2zmo',
      authDomain: 'gash-dev.firebaseapp.com',
      databaseURL: 'https://gash-dev.firebaseio.com',
      storageBucket: 'gash-dev.appspot.com',
      messagingSenderId: '567326611831'};

    var stagingConfig = {
    apiKey: 'AIzaSyAtPA0wzHtwPTOD032Omd9VnuHHpUf59nE',
    authDomain: 'gash-cr-staging.firebaseapp.com',
    databaseURL: 'https://gash-cr-staging.firebaseio.com',
    storageBucket: 'gash-cr-staging.appspot.com',
    messagingSenderId: '1025441099294'};

    var prodConfig = {
    apiKey: 'AIzaSyCO6BW3U1P4WZ-AUIEMZBK-LKiY6teDabQ',
    authDomain: 'gash-26f93.firebaseapp.com',
    databaseURL: 'https://gash-26f93.firebaseio.com',
    storageBucket: 'gash-26f93.appspot.com',
    messagingSenderId: '481747666708'};

    $window.firebase.initializeApp(stagingConfig);
  }
})();
