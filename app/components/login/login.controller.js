(function() {
  'use strict';

  angular
    .module('app.login')
    .controller('LoginController', LoginController);

  /* @ngInject */
  function LoginController(loginService, $state, toastr, sessionService) {
    var vm = this;
    vm.authenticateUser = authenticateUser;
    vm.redirectToHome = redirectToHome;
    vm.showError = showError;
    activate();

    function activate() {
      vm.user = {};
    }

    function authenticateUser() {
      loginService.logIn(vm.user.email, vm.user.password)
        .then(redirectToHome)
        .catch(showError);
    }

    function showError() {
      toastr.error('Usuario o contraseña incorrectos', 'Autentificación Incorrecta');
    }

    function redirectToHome(user) {
      if (user) {
        sessionService.setAuthData(user.email);
        $state.go('home.listRevisions');
      }
    }
  }
})();
