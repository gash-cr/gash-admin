(function() {
  'use strict';

  angular
    .module('app.core')
    .factory('loginService', loginService);

  /* @ngInject */
  function loginService(sessionService, $rootScope, $location) {

    var service = {
      logIn: logIn,
      logOut: logOut,
      verifyAccess: verifyAccess,
      isLoggedIn: isLoggedIn
    };

    return service;

    function logIn(email, pass) {
      return firebase.auth().signInWithEmailAndPassword(email, pass);
    }

    function logOut() {
      return firebase.auth().signOut();
    }

    function isLoggedIn() {
      var authData = sessionService.getAuthData();
      var sessionDefined = typeof authData !== 'undefined';
      var authDataDefined = authData !== null;
      return sessionDefined && authDataDefined;
    }

    function verifyAccess() {
      $rootScope.$on('$stateChangeStart', function(event, toState, toParams) {
        if (!service.isLoggedIn() && toState.name !== 'login') {
          $location.path('login');
        }
      });
    }
  }
})();
