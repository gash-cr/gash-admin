(function() {
  'use strict';

  angular
    .module('app.revisions')
    .controller('ListRevisionsController', ListRevisionsController);

  /* @ngInject */
  function ListRevisionsController(revisionsService, $state) {
    var vm = this;
    vm.loadRevisions = loadRevisions;
    vm.getRevision = getRevision;
    vm.sort = {
      property: 'timestamp',
      reverse: false
    };
    vm.loading = true;
    vm.sortBy = sortBy;

    activate();

    function activate() {
      revisionsService.getRevisions()
        .then(loadRevisions);
    }

    function loadRevisions(revisions) {
      vm.revisions = revisions;
      vm.loading = false;
      sortBy('timestamp');
    }

    function sortBy(propertyName) {
      vm.sort.reverse = (vm.sort.property === propertyName) ? !vm.sort.reverse : false;
      vm.sort.property = propertyName;
    }

    function getRevision(revision) {
      switch (revision.shipping_co) {
        case 'gash':
          goToShowGashRevision(revision.$id);
          break;
        case 'nyk':
          goToShowNykRevision(revision.$id);
          break;
        case 'evergreen':
          goToShowEvergreenRevision(revision.$id);
          break;
      }
    }

    function goToShowGashRevision(revisionId) {
      $state.go('home.showGashRevision', {revisionId: revisionId});
    }

    function goToShowNykRevision(revisionId) {
      $state.go('home.showNykRevision', {revisionId: revisionId});
    }

    function goToShowEvergreenRevision(revisionId) {
      $state.go('home.showEvergreenRevision', {revisionId: revisionId});
    }
  }
})();
