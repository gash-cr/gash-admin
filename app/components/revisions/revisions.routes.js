(function() {
  'use strict';

  angular
    .module('app.revisions')
    .config(routesConfiguration);

  /* @ngInject */
  function routesConfiguration($stateProvider) {
    $stateProvider
      .state('home.listRevisions', {
        url: '/revisions',
        templateUrl: 'app/components/revisions/list/revisions.list.html',
        controller: 'ListRevisionsController as vm'
      })
      .state('home.showGashRevision', {
        url: '/gash-revision/:revisionId',
        templateUrl: 'app/components/revisions/show/gash/revision.gash.show.html',
        controller: 'showRevisionController as vm'
      })
      .state('home.showNykRevision', {
        url: '/nyk-revision/:revisionId',
        templateUrl: 'app/components/revisions/show/nyk/revision.nyk.show.html',
        controller: 'showRevisionController as vm'
      })
      .state('home.showEvergreenRevision', {
        url: '/evergreen-revision/:revisionId',
        templateUrl: 'app/components/revisions/show/evergreen/revision.evergreen.show.html',
        controller: 'showRevisionController as vm'
      })
      .state('home.damageGashRevision', {
        url: '/gash-damage/:damageId&:revisionId',
        templateUrl: 'app/components/revisions/damages/gash/revision.gash.damage.html',
        controller: 'damageRevisionController as vm'
      })
      .state('home.damageNykRevision', {
        url: '/nyk-damage/:damageId&:revisionId',
        templateUrl: 'app/components/revisions/damages/nyk/revision.nyk.damage.html',
        controller: 'damageRevisionController as vm'
      })
      .state('home.damageEvergreenRevision', {
        url: '/evergreen-damage/:damageId&:revisionId',
        templateUrl: 'app/components/revisions/damages/evergreen/revision.evergreen.damage.html',
        controller: 'damageRevisionController as vm'
      });
  }
})();
