(function() {

  angular
    .module('app.revisions')
    .controller('RevisionPDFModalController', RevisionPDFModalController);

  /* @ngInject */
  function RevisionPDFModalController($uibModalInstance,
                                      revisionsService,
                                      revision,
                                      addAlert) {

    var vm = this;
    vm.revision = revision;
    vm.addAlert = addAlert;
    vm.generatePDF = generatePDF;
    vm.cancel = cancel;

    function generatePDF(revision) {
      vm.addAlert({type: 'info', msg: 'Se está procesando el reporte'});
      $uibModalInstance.close(revisionsService.getRevisionPDF(revision.$id));
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }

  }

})();
