(function() {
  'use strict';

  angular
    .module('app.revisions')
    .controller('showRevisionController', showRevisionController);

  /* @ngInject */
  function showRevisionController(revisionsService,
                                  $state,
                                  $window,
                                  $uibModal,
                                  CORE) {

    var vm = this;
    vm.alerts = [];
    vm.addAlert = addAlert;
    vm.loadRevision = loadRevision;
    vm.revisionId = $state.params.revisionId;
    vm.goToGashDamage = goToGashDamage;
    vm.goToEvergreenDamage = goToEvergreenDamage;
    vm.goToNykDamage = goToNykDamage;
    vm.printRevision = printRevision;
    vm.getItem = getItem;
    vm.sort = {
      property: 'name',
      reverse: false
    };
    vm.sortBy = sortBy;

    activate();

    function activate() {
      revisionsService.getRevision(vm.revisionId)
      .then(loadRevision);
    }

    function addAlert(alert) {
      vm.alerts.push(alert);
    }

    function closeAlert(index) {
      vm.alerts.splice(index, 1);
    }

    function printRevision() {
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'app/components/revisions/show/pdf/revision-modal-pdf.html',
        controller: 'RevisionPDFModalController',
        controllerAs: 'vm',
        size: 'sm',
        resolve: {
          revision: function() {
            return vm.revision;
          },
          addAlert: function() {
            return vm.addAlert;
          }
        }
      });

      modalInstance.result
        .then(function(success) {
          var file = new Blob([success.data], {type: 'application/pdf'});
          var fileURL = URL.createObjectURL(file);
          window.open(fileURL);
          vm.addAlert({type: 'success', msg: 'Se ha generado el pdf correctamente'});
        })
        .catch(function(error) {
          if (error != 'cancel' && error != 'backdrop click') {
            vm.addAlert({type: 'danger', msg: 'No se ha podido obtener el PDF.'});
          }
        });
    }

    function loadRevision(revision) {
      areTiresSealEmpty(revision);
      areSealsEmpty(revision);
      vm.revision = revision;
      vm.parts = revision.parts;
    }

    function areTiresSealEmpty(revision) {
      revision.tire_seal_1 = revision.tire_seal_1 ? revision.tire_seal_1 : 'N/A';
      revision.tire_seal_2 = revision.tire_seal_2 ? revision.tire_seal_2 : 'N/A';
      revision.tire_seal_3 = revision.tire_seal_3 ? revision.tire_seal_3 : 'N/A';
      revision.tire_seal_4 = revision.tire_seal_4 ? revision.tire_seal_4 : 'N/A';
      revision.tire_seal_5 = revision.tire_seal_5 ? revision.tire_seal_5 : 'N/A';
      revision.tire_seal_6 = revision.tire_seal_6 ? revision.tire_seal_6 : 'N/A';
    }

    function areSealsEmpty(revision) {
      revision.seal_no_1 = revision.seal_no_1 ? revision.seal_no_1 : 'N/A';
      revision.seal_no_2 = revision.seal_no_2 ? revision.seal_no_2 : 'N/A';
      revision.seal_no_3 = revision.seal_no_3 ? revision.seal_no_3 : 'N/A';
      revision.seal_no_4 = revision.seal_no_4 ? revision.seal_no_4 : 'N/A';
      revision.seal_no_5 = revision.seal_no_5 ? revision.seal_no_5 : 'N/A';
      revision.seal_no_6 = revision.seal_no_6 ? revision.seal_no_6 : 'N/A';
    }

    function getItem(revision) {
      return revision;
    }

    function sortBy(propertyName) {
      vm.sort.reverse = (vm.sort.property === propertyName) ? !vm.sort.reverse : false;
      vm.sort.property = propertyName;
    }

    function goToGashDamage() {
      $state.go('home.damageGashRevision', {damageId: vm.revision.damages_ref, revisionId: vm.revisionId});
    }

    function goToEvergreenDamage() {
      $state.go('home.damageEvergreenRevision', {damageId: vm.revision.damages_ref, revisionId: vm.revisionId});
    }

    function goToNykDamage() {
      $state.go('home.damageNykRevision', {damageId: vm.revision.damages_ref, revisionId: vm.revisionId});
    }

  }
})();
