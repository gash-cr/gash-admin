(function() {
  'use strict';

  angular
    .module('app.revisions')
    .factory('revisionsService', revisionsService);

  /* @ngInject */
  function revisionsService($firebaseArray, 
                            $firebaseObject, 
                            $http,
                            CORE) {

    var rootRef = firebase.database().ref();
    var service = {
      url: CORE.API_URL,
      getRevisions: getRevisions,
      getRevision: getRevision,
      getDamage: getDamage,
      getDamagePhoto: getDamagePhoto,
      getRevisionPDF: getRevisionPDF
    };

    return service;

    function getRevisions() {
      var revisionsRef = rootRef.child('revisions').limitToLast(30);
      return $firebaseArray(revisionsRef).$loaded();
    }

    function getRevision(revisionNumber) {
      var revisionRef = rootRef.child('revisions').child(revisionNumber);
      return $firebaseObject(revisionRef).$loaded();
    }

    function getRevisionPDF(revisionId) {
      var postRequest = {
        method: 'POST',
        url: service.url + 'revisions',
        headers: {
          'Content-Type': 'text/plain'
        },
        responseType: 'arraybuffer',
        data: revisionId
      };
      return $http(postRequest);
    }

    function getDamage(damageNumber, damagePlace) {
      var damageRef = rootRef.child('damages').child(damageNumber).child(damagePlace);
      return $firebaseArray(damageRef).$loaded();
    }

    function getDamagePhoto(imageRef) {
      var storageRef = firebase.storage().ref(imageRef);
      storageRef.child(imageRef).getDownloadURL()
        .then(setDamagePhoto);
    }

    function setDamagePhoto(url) {
      window.localStorage.setItem('imageURL',url);
    }
  }
})();
