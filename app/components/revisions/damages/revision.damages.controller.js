(function() {
  'use strict';

  angular
    .module('app.revisions')
    .controller('damageRevisionController', damageRevisionController);

  /* @ngInject */
  function damageRevisionController(revisionsService,
                                    $state,
                                    $uibModal,
                                    $scope,
                                    $q,
                                    $log,
                                    REVISION_PARTS) {

    var vm = this;
    var isLoading = true;
    vm.damageId = $state.params.damageId;
    vm.revisionId = $state.params.revisionId;
    vm.displayDamages = displayDamages;
    vm.displayImageDamage = displayImageDamage;
    vm.imageExist = imageExist;
    vm.parts = {};
    vm.imageSrc = null;

    activate();

    function activate() {
      revisionsService.getRevision(vm.revisionId)
        .then(handleSuccessRevision)
        .catch(handleError);
    }

    function handleSuccessRevision(revision) {
      vm.revision = revision;
      var _parts = angular.fromJson(angular.toJson(REVISION_PARTS));
      setRevisionParts(_parts);
    }

    function handleError(error) {
      $log.error(error);
    }

    function setRevisionParts(parts) {
      vm.parts.left = parts[0].name;
      vm.parts.right = parts[1].name;
      vm.parts.inside = parts[2].name;
      vm.parts.leftContainer = parts[3].name;
      vm.parts.rightContainer = parts[4].name;
      vm.parts.front = parts[5].name;
      vm.parts.roof = parts[6].name;
      vm.parts.chassis_front = parts[7].name;
      vm.parts.chassis_up = parts[8].name;
      vm.parts.chassis_back = parts[9].name;
      vm.parts.chassis_side = parts[10].name;
      vm.parts.floor = parts[11].name;
      vm.parts.back = parts[12].name;
    }

    function displayDamages(event) {
      getPartTitle(event.currentTarget.id)
        .then(function(partName) {
          vm.title = partName;
          vm.imageSrc = event.currentTarget.getAttribute('data-imgSrc');
          revisionsService.getDamage(vm.damageId, event.currentTarget.id)
            .then(loadDamage);
        });

    }

    function getPartTitle(partId) {
      var defer = $q.defer();
      angular.forEach(REVISION_PARTS, function(part) {
        if (part.id == partId) {
          defer.resolve(part.name);
        }
      });
      return defer.promise;
    }

    function loadDamage(damages) {
      vm.damages = [];
      loadImageDamage();
      angular.forEach(damages, function(damage) {
        vm.damages.push({'id': damage.$id, 'name': damage.name, 'img_ref': damage.img_ref});
        addDamageSymbol(damage.relative_cords.x_percentage, damage.relative_cords.y_percentage, damage.abbreviation);
      });
    }

    function loadImageDamage() {
      initPaperLibrary();
      initializeRaster();
    }

    function initializeRaster() {
      var raster = new paper.Raster({source: vm.imageSrc, position: view.center});
      var heightScale = (paper.view.size.height / raster.height);
      var widthScale = (paper.view.size.width / raster.width);
      raster.scale(widthScale, heightScale);
    }

    function addDamageSymbol(xValue, yValue, abbreviation) {
      var xPosition = (parseFloat(xValue) * (paper.view.size.width));
      var yPosition = (parseFloat(yValue) * (paper.view.size.height));
      var text = new PointText(new Point(xPosition, yPosition));
      text.justification = 'center';
      text.fillColor = 'red';
      text.content = abbreviation;
      text.fontWeight = 'bold';
      text.fontSize = 15;
    }

    function initPaperLibrary() {
      paper.install(window);
      paper.setup('canvas');
    }

    function displayImageDamage(damageImageRef) {
      $uibModal.open({
          templateUrl: 'app/components/revisions/damages/show-image-damage/image-damage.show.html',
          controller: 'ImageDamageController as vm',
          resolve: {
            damageRef: function() {
              return damageImageRef;
            }
          }
        });
    }

    function imageExist(damageImageRef) {
      return angular.isDefined(damageImageRef);
    }
  }
})();
