(function() {
  'use strict';

  angular
    .module('app.revisions')
    .constant('REVISION_PARTS', [
      {id: 'left', 'name': 'Lado Izquierdo'},
      {id: 'right', 'name': 'Lado Derecho'},
      {id: 'inside', 'name': 'Contenedor Interior'},
      {id: 'left', 'name': 'Contenedor Izquierdo'},
      {id: 'right', 'name': 'Contenedor Derecho'},
      {id: 'front', 'name': 'Frente'},
      {id: 'roof', 'name': 'Techo'},
      {id: 'chassis_front', 'name': 'Chasis - Frente'},
      {id: 'chassis_up', 'name': 'Chasis - Arriba'},
      {id: 'chassis_back', 'name': 'Chasis - Atrás'},
      {id: 'chassis_side', 'name': 'Chasis - Costado'},
      {id: 'floor', 'name': 'Suelo'},
      {id: 'back', 'name': 'Parte Trasera'}
    ]);
})();
