
(function() {
  'use strict';

  angular
    .module('app.revisions')
    .controller('ImageDamageController', ImageDamageController);

  /* @ngInject */
  function ImageDamageController($uibModalInstance,
                                 damageRef) {

    var vm = this;
    vm.cancel = cancel;

    activate();

    function activate() {
      vm.srcImage = 'data:image/jpeg;base64,' + damageRef;
    }

    function cancel() {
      $uibModalInstance.dismiss('cancel');
    }
  }

})();
