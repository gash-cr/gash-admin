(function() {
  'use strict';

  angular
    .module('app.login')
    .controller('NavController', NavController);

  /* @ngInject */
  function NavController(loginService,
                         $state,
                         sessionService) {

    var vm = this;
    vm.redirectToLogin = redirectToLogin;
    vm.logOut = logOut;

    function logOut() {
      loginService.logOut()
        .then(redirectToLogin);
    }

    function redirectToLogin() {
      $state.go('login');
      sessionService.destroy();
    }

  }

})();
